/*******************************************************************************************
*
*   raylib [core] example - Basic window
*
*   Welcome to raylib!
*
*   To test examples, just press F6 and execute raylib_compile_execute script
*   Note that compiled executable is placed in the same folder as .c file
*
*   You can find all basic examples on C:\raylib\raylib\examples folder or
*   raylib official webpage: www.raylib.com
*
*   Enjoy using raylib. :)
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (@raysan5)
*
********************************************************************************************/

#include "raylib.h"
#include "raygui.h"
#include <iostream>

#define RAYGUI_IMPLEMENTATION
#define RAYGUI_SUPPORT_ICONS
#include "raygui.h"
#include "node.h"

node* root;
node* Blank;
Rectangle addButton;
Rectangle deleteButton;
Rectangle increaseSizeButton;
Rectangle decreaseSizeButton;
Rectangle heatButton;
Rectangle randButton;
Rectangle textBox;
Rectangle searchButton;
int spacing;
int nodeSize = 25;
int inputValue = 0;
bool empty = true;

int main(int argc, char* argv[])
{
	// Initialization
	//--------------------------------------------------------------------------------------
	int screenWidth = 1600;
	int screenHeight = 900;
	addButton = { screenWidth * 0.02f, screenHeight * 0.02f, screenWidth * 0.08f, screenHeight * 0.08f };
	deleteButton = { addButton.x + addButton.width + screenWidth * 0.01f, addButton.y, screenWidth * 0.08f, screenHeight * 0.08f };
	increaseSizeButton = { deleteButton.x + deleteButton.width + screenWidth * 0.01f, deleteButton.y, screenWidth * 0.04f, screenHeight * 0.08f };
	decreaseSizeButton = { increaseSizeButton.x + increaseSizeButton.width, increaseSizeButton.y, screenWidth * 0.04f, screenHeight * 0.08f };
	heatButton = { decreaseSizeButton.x + decreaseSizeButton.width + screenWidth * 0.01f, decreaseSizeButton.y, screenWidth * 0.08f, screenHeight * 0.08f };

	randButton = { addButton.x, heatButton.y + screenHeight * 0.1f, screenWidth * 0.07f, screenHeight * 0.06f };
	textBox = { addButton.x + addButton.width + screenWidth * 0.01f, randButton.y, screenWidth * 0.08f, screenHeight * 0.06f };
	searchButton = { textBox.x + textBox.width + screenWidth * 0.01f, textBox.y, screenWidth * 0.08f, screenHeight * 0.06f };

	root = new node(50, screenHeight, screenWidth);
	root->setPos(screenWidth * 0.5, screenHeight * 0.1);

	Blank = new node(-1, 0, 0); //used for null and checking, another way maybe?
	Blank->declareBlank(Blank);
	root->declareBlank(Blank);

	InitWindow(screenWidth, screenHeight, "Binary Tree");

	SetTargetFPS(60);
	//--------------------------------------------------------------------------------------

	// Main game loop
	while (!WindowShouldClose())    // Detect window close button or ESC key
	{
		// Update
		//----------------------------------------------------------------------------------

		if (CheckCollisionPointRec(GetMousePosition(), addButton) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
		{
			if (empty)
			{
				root->setData(inputValue);
				empty = false;
			}
			else if (!root->add(new node(inputValue, screenHeight, screenWidth), screenWidth * 0.25))
				std::cout << "Error adding entry: node will not fit on screen" << std::endl;
		}
		if (CheckCollisionPointRec(GetMousePosition(), deleteButton) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
		{
			node* targetDelete = root->search(inputValue);

			if (targetDelete != Blank && targetDelete != nullptr && targetDelete->getData() != -1)
				targetDelete->destroyAll();

			if (targetDelete == root)
			{
				root->setData(0);
				empty = true;
			}
		}
		if (CheckCollisionPointRec(GetMousePosition(), increaseSizeButton) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
		{
			nodeSize++;
		}
		if (CheckCollisionPointRec(GetMousePosition(), decreaseSizeButton) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
		{
			nodeSize--;
		}
		if (CheckCollisionPointRec(GetMousePosition(), heatButton) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
		{
			root->colourAll();
			root->heat(root->searchHeat());
		}
		if (CheckCollisionPointRec(GetMousePosition(), randButton) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
		{
			if (root->getData() > 0)
			inputValue = rand() % (root->getData() * 2);
		}
		if (CheckCollisionPointRec(GetMousePosition(), searchButton) && IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
		{
			root->colourAll();
			root->search(inputValue)->setColour(DARKGREEN);
		}

		// Draw
		//----------------------------------------------------------------------------------
		BeginDrawing();
		ClearBackground(RAYWHITE);


		//Check Buttons
		//Num input
		GuiValueBox(textBox, "Num", &inputValue, 0, 1000, true);
		if (inputValue < 0)
			inputValue = 0;
		if (inputValue > 1000)
			inputValue = 1000;

		//Add
		if (CheckCollisionPointRec(GetMousePosition(), addButton))
			DrawRectangleRec(addButton, DARKGRAY);
		else
			DrawRectangleRec(addButton, GRAY);
		DrawText("Add", addButton.x + (addButton.width / 2) - MeasureText("Add", screenWidth * 0.025) / 2, addButton.y + (addButton.height / 2) - (screenWidth * 0.025 / 2), screenWidth * 0.025, BLACK);

		//delete
		if (CheckCollisionPointRec(GetMousePosition(), deleteButton))
			DrawRectangleRec(deleteButton, MAROON);
		else
			DrawRectangleRec(deleteButton, RED);
		DrawText("Delete", deleteButton.x + (deleteButton.width / 2) - MeasureText("Delete", screenWidth * 0.02) / 2, deleteButton.y + (deleteButton.height / 2) - (screenWidth * 0.02 / 2), screenWidth * 0.02, BLACK);

		//Inc size
		if (CheckCollisionPointRec(GetMousePosition(), increaseSizeButton))
			DrawRectangleRec(increaseSizeButton, DARKGREEN);
		else
			DrawRectangleRec(increaseSizeButton, GREEN);
		DrawText("+", increaseSizeButton.x + (increaseSizeButton.width / 2) - MeasureText("+", screenWidth * 0.025) / 2, increaseSizeButton.y + (increaseSizeButton.height / 2) - (screenWidth * 0.025 / 2), screenWidth * 0.025, BLACK);

		//Dec size
		if (CheckCollisionPointRec(GetMousePosition(), decreaseSizeButton))
			DrawRectangleRec(decreaseSizeButton, DARKBLUE);
		else
			DrawRectangleRec(decreaseSizeButton, BLUE);
		DrawText("-", decreaseSizeButton.x + (decreaseSizeButton.width / 2) - MeasureText("-", screenWidth * 0.025) / 2, decreaseSizeButton.y + (decreaseSizeButton.height / 2) - (screenWidth * 0.025 / 2), screenWidth * 0.025, BLACK);

		//Heat
		if (CheckCollisionPointRec(GetMousePosition(), heatButton))
			DrawRectangleRec(heatButton, DARKGRAY);
		else
			DrawRectangleRec(heatButton, GRAY);
		DrawText("Heat", heatButton.x + (heatButton.width / 2) - MeasureText("Heat", screenWidth * 0.025) / 2, heatButton.y + (heatButton.height / 2) - (screenWidth * 0.025 / 2), screenWidth * 0.025, BLACK);

		//Randomise input
		if (CheckCollisionPointRec(GetMousePosition(), randButton))
			DrawRectangleRec(randButton, DARKGRAY);
		else
			DrawRectangleRec(randButton, GRAY);
		DrawText("Rand", randButton.x + (randButton.width / 2) - MeasureText("Rand", screenWidth * 0.025) / 2, randButton.y + (randButton.height / 2) - (screenWidth * 0.025 / 2), screenWidth * 0.025, BLACK);

		//Search
		if (CheckCollisionPointRec(GetMousePosition(), searchButton))
			DrawRectangleRec(searchButton, DARKGRAY);
		else
			DrawRectangleRec(searchButton, GRAY);
		DrawText("Search", searchButton.x + (searchButton.width / 2) - MeasureText("Search", screenWidth * 0.025) / 2, searchButton.y + (searchButton.height / 2) - (screenWidth * 0.025 / 2), screenWidth * 0.025, BLACK);

		root->draw(nodeSize);

		EndDrawing();
		//----------------------------------------------------------------------------------
	}

	// De-Initialization
	//--------------------------------------------------------------------------------------   
	CloseWindow();        // Close window and OpenGL context
	//--------------------------------------------------------------------------------------

	return 0;
}