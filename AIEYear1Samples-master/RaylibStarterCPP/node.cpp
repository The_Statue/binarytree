#include "node.h"
#include "raylib.h"
#include <iostream>

void node::setParent(node* parent)
{
	Parent = parent;
}
node* node::getParent()
{
	return Parent;
}
void node::setLesser(node* lesser)
{
	ChildLesser = lesser;
}
node* node::getLesser()
{
	return ChildLesser;
}
void node::setGreater(node* greater)
{
	ChildGreater = greater;
}
node* node::getGreater()
{
	return ChildGreater;
}
void node::setData(int d)
{
	data = d;
}
int node::getData()
{
	return data;
}
void node::setPos(int x, int y)
{
	posx = x;
	posy = y;
}
void node::draw(int size)
{
	if (ChildGreater != Blank)
	{
		DrawLineEx({ (float)posx, (float)posy }, { (float)ChildGreater->posx, (float)ChildGreater->posy }, size * 0.3, DARKGRAY);
		ChildGreater->draw(size);
	}
	if (ChildLesser != Blank)
	{
		DrawLineEx({ (float)posx, (float)posy }, { (float)ChildLesser->posx, (float)ChildLesser->posy }, size * 0.3, DARKGRAY);
		ChildLesser->draw(size);
	}
	DrawCircle(posx, posy, size, nodeColor);
	DrawText(FormatText("%i", data), posx - MeasureText(FormatText("%i", data), size) / 2, posy - size / 2, size, WHITE);
}
bool node::add(node* child, int spacing)
{
	child->declareBlank(Blank);
	bool status = true; //assume no errors
	if (posy + screenHeight * 0.08 > screenHeight&& child->getData() != data)
	{
		delete[] child;
		return false; //Node attempted to spawn offscreen, idk how to fix positioning
	}


	if (child->getData() < data)
	{
		if (ChildLesser != Blank)
			status = ChildLesser->add(child, spacing * 0.5);
		else
		{
			if (posx - spacing < 0)
				return false;
			ChildLesser = child;

			ChildLesser->setPos(posx - spacing, posy + screenHeight * 0.08);
			ChildLesser->Parent = this;
			std::cout << "Lesser(" << child->data << "): " << posx << " " << spacing << std::endl;
		}
	}
	else if (child->getData() > data)
	{
		if (ChildGreater != Blank)
			status = ChildGreater->add(child, spacing * 0.5);
		else
		{
			if (posx + spacing > screenWidth)
				return false;
			ChildGreater = child;

			ChildGreater->setPos(posx + spacing, posy + screenHeight * 0.08);
			ChildGreater->Parent = this;
			std::cout << "Greater(" << child->data << "): " << posx << " " << spacing << std::endl;
		}
	}
	else
	{
		delete[] child;
		//deal with matches?
		count++;

		std::cout << "Match found at node with value: " << data << std::endl;
	}
	return status;
}
node* node::search(int value)
{
	if (data == value)
	{
		std::cout << "Found value: " << data << std::endl;
		return this;
	}

	node* result = Blank;
	node* tempResult = Blank;

	if (ChildGreater != Blank)
	{
		tempResult = ChildGreater->search(value);
		if (tempResult != Blank)
			result = tempResult;
	}
	if (ChildLesser != Blank)
	{
		tempResult = ChildLesser->search(value);
		if (tempResult != Blank)
			result = tempResult;
	}

	return result;
}
int node::searchHeat()
{
	int max = count;
	if (ChildGreater != Blank)
	{
		int greaterMax = ChildGreater->searchHeat();
		if (max < greaterMax)
			max = greaterMax;
	}
	if (ChildLesser != Blank)
	{
		int lesserMax = ChildLesser->searchHeat();
		if (max < lesserMax)
			max = lesserMax;
	}
	return max;
}
void node::heat(int heat)
{
	nodeColor.r = ((float)count / heat) * 255;

	if (ChildGreater != Blank)
		ChildGreater->heat(heat);
	if (ChildLesser != Blank)
		ChildLesser->heat(heat);
}
void node::destroyAll()
{
	if (this == Blank && this == nullptr && this->getData() == -1)
	{
		std::cout << "Error: attempted delete of blank node" << std::endl;
		return;
	}
	std::cout << "Staring deletion of node with value: " << data << std::endl;

	if (Parent->ChildGreater == this)
		Parent->ChildGreater = Parent->Blank;
	if (Parent->ChildLesser == this)
		Parent->ChildLesser = Parent->Blank;

	if (ChildGreater != Blank)
		ChildGreater->destroyAll();
	if (ChildLesser != Blank)
		ChildLesser->destroyAll();

	std::cout << "Finalized deletion of node with value: " << data << std::endl;
	if (Parent != Blank)
		delete[] this;
	else
		std::cout << "Failed to delete node: assumed root";
}
void node::colourAll()
{
	if (this == Blank && this == nullptr && this->getData() == -1)
	{
		std::cout << "Error: attempted colour of blank node" << std::endl;
		return;
	}

	if (ChildGreater != Blank)
		ChildGreater->colourAll();
	if (ChildLesser != Blank)
		ChildLesser->colourAll();

	nodeColor = BLACK;
}
void node::setColour(Color input)
{
	nodeColor = input;
}
void node::declareBlank(node* blank)
{
	Parent = blank;
	Blank = blank;
	ChildGreater = blank;
	ChildLesser = blank;
}