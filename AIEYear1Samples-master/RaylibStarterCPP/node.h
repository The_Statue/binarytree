#pragma once
#include "raylib.h"
struct node
{
public:
	void setParent(node* parent);
	node* getParent();
	void setLesser(node* lesser);
	node* getLesser();
	void setGreater(node* greater);
	node* getGreater();
	void setData(int d);
	int getData();
	void setPos(int x, int y);
	void draw(int size);
	bool add(node* child, int spacing);
	node* search(int value);
	int searchHeat();
	void heat(int heat);
	void destroyAll();
	void colourAll();
	void setColour(Color input);
	void declareBlank(node* blank);
	node(int d, int height, int width)
	{
		data = d;
		screenHeight = height;
		screenWidth = width;
	};

private:
	node* Parent;
	node* ChildLesser;
	node* ChildGreater;
	node* Blank; //used as a check for blank nodes, different method to check? nullptr doesnt work?
	int data = -1;
	int count = 1;
	int screenHeight = -1;
	int screenWidth = -1;
	int posx = 0;
	int posy = 0;
	Color nodeColor = BLACK;
};

